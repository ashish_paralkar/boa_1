#### Checking history of changes `git log`
```
fatal: your current branch 'master' does not have any commits yet
```
#### Committing the changes `git commit -m "added first file with first change for the day"`
```
[master (root-commit) 3fa9cce] added first file with first change for the day
 1 file changed, 62 insertions(+)
 create mode 100644 README.md
```
#### Check the status `git status`
```
On branch master
nothing to commit, working tree clean
```
#### Checking history of changes `git log`
```
commit 3fa9ccecd510783a99bc60a6490ac24e2dce4246 (HEAD -> master)
Author: Kulbhushan Mayer <kulbhushan.mayer@gmail.com>
Date:   Sun Mar 13 11:19:58 2022 +0530

    added first file with first change for the day
```
#### Get limited output for the histroy `git log --oneline`
```
3fa9cce (HEAD -> master) added first file with first change for the day
```
#### Added few more change to the `README.md`
#### Check the status `git status`
```
On branch master
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
        modified:   README.md
		
#changes done

no changes added to commit (use "git add" and/or "git commit -a")
```
#### Stage changes: `git add README.md`
#### Commit changes: `git commit -m "some relevent message"`